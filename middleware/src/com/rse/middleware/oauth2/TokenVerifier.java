/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rse.middleware.oauth2;

/**
 *
 * @author Ichlasul Affan
 */
public interface TokenVerifier {
    TokenPayload verify(final String clientId, final String token);
}
